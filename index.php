<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

	<head>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
		
		<link href="estilos/reset.css" type="text/css" rel="stylesheet">
		<link href="estilos/estilos.css" type="text/css" rel="stylesheet">

		<!-- Materialize.css -->
		<link href="css/materialize.min.css" type="text/css" rel="stylesheet">

		<!-- Favicon -->
		<link rel="icon" type="image/png" href="favicon.png">
		
		<!--[if lt IE 9]>
		    <script src="scripts/html5shiv.js"></script>
		<![endif]-->

		<title>Ferias Jerez</title>

	</head>

	<body>

		<!-- Header logo -->
		<header class="row fixed-top">

			<div class="float-30">
				
				<a class="white-text bolder-text hide-on-small-only handed botonRegistros">Registrate</a>
				<a class="btn-floating btn-medium waves-effect waves-light blue darken-1 botonRegistros"><i class="material-icons">directions_walk</i></a>
			
			</div>

			<div class="float-60">

				<a class="white-text bolder-text hide-on-small-only handed botonCartel">Cartel Oficial</a>
				<a class="btn-floating btn-medium waves-effect waves-light blue darken-4 botonCartel"><i class="material-icons">today</i></a>

			</div>
								
			<div class="col s12 m8 l6 offset-m2 offset-l3">

    			<img class="col s12 m8 l6 offset-m4 offset-l6 responsive-img" src="img/headerLogo.png">

        				
			</div>

		</header>


		<main>

			<!-- Info section -->
			<div class="row fixed-250">

				<div class="col s12 m8 l6 offset-m2 offset-l3">

					<div class="col s12 m12 l12">
						
						<div class="card transparent z-depth-0">
							
							<div class="card-content white-text">
								
								<span class="card-title"><h3 class="bolder-text">REGISTRATE</h3></span>
								
								<p class="flow-text ligth-text p-format">Nos pondremos en contacto contigo para apartarte uno de los mejores reservados de la <b><u>FERIA DE PRIMAVERA JEREZ 2018</u></b></p>

							</div>

						</div>

					</div>          
					
				</div>

			</div>


			<!-- Contact mosaic -->
			<div class="row padding-footer">


				<div class="col s12 m12 l12 height-750" style="position: relative;">

					<div class="col s12 m6 l6 pink darken-1 z-depth-0 valign-wrapper height-50">


						<ul style="width: 100%; text-align: center;">

							<li class="section"><h5 class="flow-text white-text">reservaciones<b>@</b>feriasjerez.club</h4></li>

							<li style="display: inline-block;">
								<a href="https://www.facebook.com/feriasjerez/" target="_blank">
									<img class="responsive-img" src="img/facebook.png" width="60px">
								</a>
							</li>
							<li style="display: inline-block;">
								<a href="https://twitter.com/JeresFeriasClub" target="_blank">
									<img class="responsive-img" src="img/twitter.png" width="60px">
								</a>
							</li>
							<li style="display: inline-block;">
								<a href="https://www.instagram.com/feriasjerezclub/" target="_blank">
									<img class="responsive-img" src="img/instagram.png" width="60px">
								</a>
							</li>

						</ul>

					</div>

					<div class="col s12 m6 l6 white z-depth-0 height-50 img-square-one" style="padding: 0;">
					
						

					</div>

					<div class="col s12 m6 l6 white z-depth-0 height-50 img-square-two" style="padding: 0;">
					
				

					</div>

					<div class="col s12 m6 l6 black z-depth-0 valign-wrapper height-50" style="position: relative;">

						<div id="registroPin"></div>

						<form id="enviaRegistro" class="col s12" action="php/sendRegistro.php" method="POST" enctype="multipart/form-data">
						
							<div class="input-field col s12 white-text">

								<input class="white-text" id="first_name" type="text" class="validate" autocomplete="off" name="nameReg">
								<label for="first_name">Nombre</label>
							
							</div>
							
							<div class="input-field col s6" white-text>
								
								<input class="white-text" id="last_name" type="text" class="validate" autocomplete="off" name="telReg">
								<label for="last_name">Teléfono</label>
							
							</div>
							
							<div class="input-field col s6" white-text>
								
								<input class="white-text" id="email" type="email" class="validate" autocomplete="off" name="mailReg">
								<label for="email">Email</label>
							
							</div>
							
							<div class="input-field col s6" white-text>
							
								<input class="white-text" id="procedence" type="text" class="validate" autocomplete="off" name="oriReg">
								<label for="procedence">Lugar de Origen</label>

							</div>

							<div class="input-field col s6" white-text>
							
								<button class="btn waves-effect waves-light btn-large transparent" type="submit" name="action" style="border: 1px solid white;">Reservar
									<i class="material-icons right">send</i>
								</button>
								    

							</div>
							
						</form>

					</div> 

				</div>
				
				<div class="col s12 m12 l12">

					<div class="row" style="position: relative;">

						<div class="row"></div>

						<div id="cartelPin"></div>

						<div class="col l12 black">
							
							<h4 class="white-text">Cartel Oficial de la Feria de Primavera 2018</h4>

							<div class="col s4 m3 l2">
								<div class="section">
									<img class="materialboxed responsive-img" width="250" src="img/carousel/car03.png">
								</div>
							</div>
							<div class="col s4 m3 l2">
								<div class="section">
									<img class="materialboxed responsive-img" width="250" src="img/carousel/car07.png">
								</div>
							</div>
							<div class="col s4 m3 l2">
								<div class="section">
									<img class="materialboxed responsive-img" width="250" src="img/carousel/car05.png">
								</div>
							</div>
							<div class="col s4 m3 l2">
								<div class="section">
									<img class="materialboxed responsive-img" width="250" src="img/carousel/car01.png">
								</div>
							</div>
							<div class="col s4 m3 l2">
								<div class="section">
									<img class="materialboxed responsive-img" width="250" src="img/carousel/car09.png">
								</div>
							</div>
							<div class="col s4 m3 l2">
								<div class="section">
									<img class="materialboxed responsive-img" width="250" src="img/carousel/car10.png">
								</div>
							</div>
							<div class="col s4 m3 l2">
								<div class="section">
									<img class="materialboxed responsive-img" width="250" src="img/carousel/car08.png">
								</div>
							</div>
							<div class="col s4 m3 l2">
								<div class="section">
									<img class="materialboxed responsive-img" width="250" src="img/carousel/car06.png">
								</div>
							</div>
							<div class="col s4 m3 l2">
								<div class="section">
									<img class="materialboxed responsive-img" width="250" src="img/carousel/car04.png">
								</div>
							</div>
							<div class="col s4 m3 l2">
								<div class="section">
									<img class="materialboxed responsive-img" width="250" src="img/carousel/car02.png">
								</div>
							</div>

						</div>

					</div>

				</div>

			</div>

			<div class="row">

				<div class="col s12 m12 l12 z-depth-0">

					<video class="materialboxed responsive-video" controls style="width: 100vw;">
					    <?php
					    	echo"
					    		<source src=\"vid/vid0".generateRandomString().".mp4\" type=\"video/mp4\">
					    	";
					    ?>
					</video>

				</div>

			</div>


			<footer class="page-footer grey darken-4">

				<div class="container">

					<div class="row">
					
						<div class="col l6 s12">
						
							<h5 class="white-text">Contactanos</h5>
							<p class="grey-text text-lighten-4"><b><u class="botonRegistros handed" style="line-height: 2;">Registrate</u></b> o contactanos a <u class="botonCartel handed"><b>redes@feriasjerez.club</b></u></p>
						
						</div>
						
						<div class="col l4 offset-l2 s12">
						
							<h5 class="white-text">Redes Sociales</h5>
							
							<ul>
							
								<li><a class="grey-text text-lighten-3" href="https://www.facebook.com/feriasjerez/" target="_blank">Facebook</a></li>
								<li><a class="grey-text text-lighten-3" href="https://twitter.com/JeresFeriasClub" target="_blank">Twitter</a></li>
								<li><a class="grey-text text-lighten-3" href="https://www.instagram.com/feriasjerezclub/" target="_blank">Instagram</a></li>
							
							</ul>
							
						</div>

					</div>

				</div>

				<div class="footer-copyright blue-grey darken-4">
	            
		            <div class="container">

			            Feria de Primavera Jerez - 2018

			            <a class="grey-text text-lighten-4 right" href="http://feriasjerez.club/avisodeprivacidad.pdf" target="_blank">Aviso de Privacidad</a>

		            </div>
	          	
	          	</div>

			</footer>

		</main>


	</body>

</html>

<!-- Materialize.js -->
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>

<!-- Scripts -->
<script type="text/javascript" src="scripts/onLoad.js"></script>


<?php

	//Generador de Cadenas Aleatorias
	function generateRandomString($length = 1) {
	    $characters = '123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

?>