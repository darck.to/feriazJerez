<?php

	//Recibimos los datos y los limpiamos con la funcion
	$nombre = limpiaCadenas($_POST['nameReg']);
	$telefono = limpiaCadenas($_POST['telReg']);
	$email = limpiaCadenas($_POST['mailReg']);
	$origen = limpiaCadenas($_POST['oriReg']);
	
	$plantilla = "

		<div style=\"display:block; width: calc(100% - 20px); margin: 0; padding: 10px; font-family: Arial, Helvetica, sans-serif; border: 1px dotted #f2f2f2;\">
	
			<div style=\"display: block; text-align: center; margin: auto; width: 70%;\">
				<div style='position: relative; width: 100%;'>
					<img src=\"http://feriasjerez.club/img/graciasMail.png\" width=\"100%\">
				</div>
			</div>

			<div style=\"border: 5px solid #f2f2f2;\">

				<div style=\"background-color: #f2f2f2; margin: 0; padding: 10px;\">
					<h2>".ucfirst($nombre).", Muchas gracias por tu registro!</h2>
					<p style='line-spacing: 2; padding: 20px; text-align: justify; font-size: 1rem;'>Proximamente te estaremos enviando informacion detallada para hacer las reservaciones en los mejores lugares de la Feria de Primavera Jerez 2018</p>
				</div>

				<div style=\"margin: 0; padding: 10px;\"\">
					<h3>Por lo pronto sigamos en contacto</h3>
					<p style='line-spacing: 2; padding: 20px; text-align: justify; font-size: 1rem;'>Es importante que sepas que estas registrado como una de las primeras personas con las que nos comunicaremos para ofrecerles las mejores promociones y lugares en todos los eventos que ofreceran las proximas fechas</p>
				</div>

				<div>
					<p style='line-spacing: 1.4; padding: 25px; text-align: center; font-size: .8rem;'>Ponemos a tu disposicion nuestros canales en las redes sociales para que sigas informado y en contacto para cualquier duda o sugerencia</p>
				</div>

				<div style=\"background-color: #f2f2f2; margin: 0; padding: 10px;\">

					<ul style=\"text-align: center; padding: 0;\">

						<li style=\"display: inline-block;\">
							<a href=\"https://www.facebook.com/feriasjerez/\" target=\"_blank\">
								<img src=\"http://feriasjerez.club/img/facebook.png\" width=\"50px\">
							</a>
						</li>
						<li style=\"display: inline-block;\">
							<a href=\"https://twitter.com/JeresFeriasClub\" target=\"_blank\">
								<img src=\"http://feriasjerez.club/img/twitter.png\" width=\"50px\">
							</a>
						</li>
						<li style=\"display: inline-block;\">
							<a href=\"https://www.instagram.com/feriasjerezclub/\" target=\"_blank\">
								<img src=\"http://feriasjerez.club/img/instagram.png\" width=\"50px\">
							</a>
						</li>

					</ul>

					<div>
						<p style='line-spacing: 1.4; padding: 0 25px; text-align: center; font-size: .8rem;'><a style=\"color: #2f2f2f; text-decoration: none;\" href=\"http://feriasjerez.club/avisodeprivacidad.pdf\" target=\"_blank\">AVISO DE PRIVACIDAD</a></p>
					</div>

				</div>

			</div>

		</div>
	";


	// Enviamos por email una confirmacion de la reservacion
    $remite_nombre = "Ferias Jerez"; // Tu nombre o el de tu página
    $remite_email = "reservaciones@feriasjerez.club"; // tu correo
    $asunto = "Gracias por tu Pre/Reserva"; // Asunto (se puede cambiar)
    $mensaje = $plantilla;
    $cabeceras = "From: ".$remite_nombre." <".$remite_email.">\r\n";
    $cabeceras = $cabeceras."Mime-Version: 1.0\n";
    $cabeceras = $cabeceras."Content-Type: text/html";
    $enviar_email = mail($email,$asunto,$mensaje,$cabeceras);
    if($enviar_email) {
        echo $nombre;
    }else {
        echo "No se ha podido enviar el email. <a href='javascript:history.back();'>Reintentar</a>";
    }

    // Enviamos una confirmacion a google spreadsheets

    $emailReservaciones = "darck.to@gmail.com";
	$remite_nombre = $email; // Tu nombre o el de tu página
    $remite_email = "reservaciones@feriasjerez.club"; // tu correo
    $asunto = $telefono; // Asunto (se puede cambiar)
    $mensaje = $nombre." - Desde:".$origen;
    $cabeceras = "From: ".$remite_nombre." <".$remite_email.">\r\n";
    $cabeceras = $cabeceras."Mime-Version: 1.0\n";
    $cabeceras = $cabeceras."Content-Type: text/html";
    $enviar_email = mail($emailReservaciones,$asunto,$mensaje,$cabeceras);
    if($enviar_email) {
        echo $nombre;
    }else {
        echo "No se ha podido enviar el email. <a href='javascript:history.back();'>Reintentar</a>";
    }    



	function limpiaCadenas($value) {
	    $search = array("\\",  "\x00", "\n",  "\r",  "'",  '"', "\x1a");
	    $replace = array("\\\\","\\0","\\n", "\\r", "\'", '\"', "\\Z");

	    return str_replace($search, $replace, $value);
	}

?>