$(document).ready(function(){
  
  $('.carousel').carousel({
    fullWidth: false,
    noWrap: true
  });

  $('.materialboxed').materialbox();

});


$(".botonRegistros").click(function() {
    $('html, body').animate({
        scrollTop: $("#registroPin").offset().top
    }, 500);
});

$(".botonCartel").click(function() {
    $('html, body').animate({
        scrollTop: $("#cartelPin").offset().top
    }, 500);
});

//Guardamos los datos del formulario de captura de Jerarquias/Alias
$('#enviaRegistro').submit(function(event) {
  event.preventDefault();

  var formNombre = $(this).attr('name');
  var formData = $(this).serialize();
  var formMethod = $(this).attr('method');
  var rutaScrtip = $(this).attr('action');

  var request = $.ajax({
    url: rutaScrtip,
    method: formMethod,
    data: formData,
    dataType: "html"
  });

  // handle the responses
  request.done(function(data, req, err) {
    // update the user
    console.log(data);
    M.toast({html: 'Información recibida, nos pondremos en contacto contigo'},5000) 
  })

  request.fail(function(jqXHR, textStatus) {
    console.log(textStatus);
  })

  request.always(function(data) {
    // clear the form
    $('#enviaRegistro').trigger('reset');
  });

});